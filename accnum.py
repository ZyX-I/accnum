# vim: set fileencoding=utf-8

from math import sqrt, log10, floor
import math
from sympy import Symbol, Number
import sympy
import operator

def tosameclass(func):
    def r(self, f):
        if not isinstance(f, type(self)):
            f=type(self)(f, 0.0, Number(f))
        return func(self, f)
    return r

def fromfunc_binary(func):
    @tosameclass
    def r(self, f):
        vals = self.vals.copy()
        vals.update(f.vals)
        return type(self)(
            func(float(self), float(f)),
            None,
            func(self.nexpr, f.nexpr),
            vals,
        )
    return r

def fromfunc_unary_named(fname):
    mfunc = getattr(math, fname)
    sfunc = getattr(sympy, fname)
    def r(self):
        return type(self)(
            mfunc(float(self)),
            None,
            sfunc(self.nexpr),
            self.vals,
        )
    return r

lastsym = 'a'

def getnext(ls):
    head = ls[:-1]
    tail = ls[-1]
    otail = ord(tail)
    if ord('a') <= otail < ord('f') or ord('A') <= otail < ord('F'):
        return head + chr(otail + 1)
    elif otail >= ord('f'):  # Note: ord('F') < ord('f')
        return head + 'A'
    elif head:
        return getnext(head) + 'a'
    else:
        return 'aa'

def getsym():
    global lastsym
    ret = getnext(lastsym)
    lastsym = ret
    return ret

DELTA_PREFIX = 'D_'

class AccNum(float):
    __slots__=('acc', 'nexpr', 'asym', 'aexpr', 'vals')

    def __new__(cls, f, acc=None, nexpr=None, vals=None):
        try:
            l=list(f)
            return super(AccNum, cls).__new__(cls, sum(l)/len(l))
        except TypeError:
            return super(AccNum, cls).__new__(cls, f)

    def __init__(self, f, acc=None, nexpr=None, vals=None):
        if isinstance(f, type(self)):
            self.acc=f.acc
            self.nexpr=f.nexpr
            self.asym=f.asym
            self.aexpr=f.aexpr
            self.vals=f.vals.copy()
            return
        else:
            uselist=0
            try:
                l=list(f)
                uselist=1
            except TypeError:
                uselist=0
            if acc is not None:
                self.acc = acc
                if self.acc < 0:
                    raise ValueError('Accuracy value cannot be negative')
            elif not isinstance(nexpr, sympy.Basic):
                self.acc = 0.0
            if uselist:
                n=len(l)
                ax=sum(l)/n
                self.acc=float(sqrt(acc+sum([(x-ax)**2 for x in l])/(n*(n-1))))
        self.vals=vals.copy() if vals is not None else {}
        self.aexpr = None
        if nexpr:
            if isinstance(nexpr, sympy.Basic):
                self.nexpr = nexpr
                self.asym = None
                atoms = self.nexpr.atoms(Symbol)
                if atoms:
                    for atom in self.nexpr.atoms(Symbol):
                        val = self.vals[str(atom)]
                        el = (self.nexpr.diff(atom) * val.asym) ** 2
                        if self.aexpr is None:
                            self.aexpr = el
                        else:
                            self.aexpr += el
                    self.aexpr = sympy.sqrt(self.aexpr)
                    self.acc = float(self.aexpr.evalf(subs=self.vals))
                else:
                    self.acc = acc or 0.0
                    self.aexpr = Number(self.acc)
            else:
                asym = DELTA_PREFIX + nexpr
                self.nexpr = Symbol(nexpr)
                self.asym = Symbol(asym)
                self.vals[nexpr] = self
                self.vals[asym] = self.acc
        else:
            sym = getsym()
            asym = DELTA_PREFIX + sym
            self.nexpr = Symbol(sym)
            self.asym = Symbol(asym)
            self.vals[sym] = self
            self.vals[asym] = self.acc
        if not self.aexpr:
            self.aexpr = Number(self.acc)

    def __repr__(self):
        return '{0}({1!r}, {2!r}, sympy.Expr({3!r}))'.format(
            type(self).__name__,
            float(self),
            getattr(self, 'acc', None),
            str(self.nexpr),
        )

    def __str__(self):
        num=float(self)
        acc=self.acc
        try:
            numfmd=int(floor(log10(abs(num))))
        except ValueError:
            return '0 +- '+repr(acc)
        try:
            accfmd=int(floor(log10(abs(acc))))
        except ValueError:
            return '{0}'.format(num)
        if (1.0<=(acc/(10**accfmd))<2.0):
            accfmd-=1
        fmdiff=numfmd-accfmd
        suf=''
        if fmdiff<0:
            f='!r}'
        elif accfmd>3 or accfmd<-5:
            num/=10**numfmd
            acc/=10**numfmd
            f=':.'+str(fmdiff)+'f}'
            suf=' * 10**('+str(numfmd)+')'
        elif accfmd>0:
            f=':d}'
        elif numfmd>0:
            f=':'+str(numfmd+fmdiff+2)+'.'+str(fmdiff)+'f}'
        else:
            f=':.'+str(abs(accfmd))+'f}'
        return ('({0'+f+' +- {1'+f+')').format(num, acc)+suf

    for op in ('add', 'sub', 'mul', 'truediv', 'div', 'pow',):
        exec ('__{0}__ = fromfunc_binary(operator.__{0}__)'.format(op))
    del op

    def __rsub__(self, f):
        return (-self)+f

    @tosameclass
    def __rtruediv__(self, f):
        return f/self

    __radd__=__add__
    __rmul__=__mul__
    __rdiv__=__rtruediv__

    def __neg__(self):
        return type(self)(
            -float(self),
            self.acc,
            -self.nexpr,
            self.vals,
        )

    def __pos__(self):
        return type(self)(self)

    def __abs__(self):
        return type(self)(abs(self), self.acc, abs(self.nexpr), self.vals)

    for fname in ('sin', 'cos', 'tan', 'sinh', 'cosh', 'tanh', 'log', 'exp'):
        exec ('{0} = fromfunc_unary_named({0!r})'.format(fname))
    del fname

def readdata(s, g=None, l=None, p=True):
    atoms=s.split()
    if l is None:
        if g is None:
            l={}
            g={}
        else:
            l=g
    elif g is None:
        g=l
    while atoms:
        v = atoms.pop(0)
        if not atoms:
            break
        n = atoms.pop(0)
        isl = False
        isx = False
        e = None
        if n[0] == '[':
            isl=True
            while n[-1] != ']' and atoms:
                n+=atoms.pop(0)
        elif n[0] == '=':
            n=eval(n[1:], g, l)
            isx=True
            if p:
                print n
        else:
            if atoms:
                e=atoms.pop(0)
            else:
                e=0
        if isl:
            l[v]=AccNum(eval(n, g, l), nexpr=v)
        elif isx:
            l[v]=n
        else:
            l[v]=AccNum(float(n), float(e), v)
        if atoms and atoms[0] and atoms[0][0] == '*':
            l[v]*=float('1e'+atoms[0][1:])
            del atoms[0]
    return l
